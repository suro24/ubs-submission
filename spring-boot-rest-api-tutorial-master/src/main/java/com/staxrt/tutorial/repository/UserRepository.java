package com.staxrt.tutorial.repository;

import com.staxrt.tutorial.model.User;

import java.util.List;

import org.springframework.data.annotation.QueryAnnotation;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;


@Repository
public interface UserRepository extends JpaRepository<User, Long> {
	
@Query("Select u from users u where u.firstName= ?1")
		  User findByFirstName(String firstName);
	}
	
	
	
	

