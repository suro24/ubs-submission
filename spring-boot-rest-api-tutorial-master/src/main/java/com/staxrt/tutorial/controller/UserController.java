

package com.staxrt.tutorial.controller;

import com.staxrt.tutorial.exception.ResourceNotFoundException;
import com.staxrt.tutorial.model.User;
import com.staxrt.tutorial.repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


@RestController
@RequestMapping("/api/v1")
public class UserController {

	@Autowired
	private UserRepository userRepository;


	@GetMapping("/users")
	public List<User> getAllUsers() {
		return userRepository.findAll();
	}


	@GetMapping("/users/{id}")
	public ResponseEntity<User> getUsersById(@PathVariable(value = "id") Long userId) throws ResourceNotFoundException {
		User user = userRepository.findById(userId)
				.orElseThrow(() -> new ResourceNotFoundException("User not found on :: " + userId));
		return ResponseEntity.ok().body(user);
	}


	@GetMapping("/users/{firstName}")
	public ResponseEntity<User> getUserByFirstName(@PathVariable(value = "firstName") String fName) throws ResourceNotFoundException {
		User user = userRepository.findByFirstName(fName);
		return ResponseEntity.ok().body(user);
		
		//orElseThrow(() -> new ResourceNotFoundException("User name not found on :: " + fName));
	}

	
	
	

	@PostMapping("/users")
	public User createUser(@Valid @RequestBody User user) {
		return userRepository.save(user);
	}

}
